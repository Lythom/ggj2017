import './index.css'

import React from 'react'
import {render} from 'react-dom'

import {Provider} from 'mobx-react'

import App from 'src/demo/App'

import * as demoStore from 'src/demo/stores'
import * as mageStore from 'mage/stores'

import startLoop from './loop'
import initEvents from './initEvents'

// rendering

render(
  <Provider config={demoStore.config} ui={demoStore.ui} entities={mageStore.entities}>
    <App/>
  </Provider>,
  document.querySelector('#app'))

// main loop - responding to user inputs
startLoop()

// events
initEvents()

// dev stuff
window.printStores = function() {
  console.log("entities", JSON.stringify(mageStore.entities.toJS(), null, 2))
  console.log("ui", JSON.stringify(demoStore.ui, null, 2))
  console.log("config", JSON.stringify(demoStore.config, null, 2))
}

window.printEntity= function(id) {
  console.log("entities", JSON.stringify(mageStore.entities.get(id).toJS(), null, 2))

}

if (module.hot) {
  module.hot.accept()
}
// document.styleSheets[0].addRule('.container, .entity', 'transition: transform 60ms linear;');
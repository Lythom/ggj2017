import Keyboard from '../Keyboard'
import {JUMP, MOVE_LEFT, MOVE_RIGHT} from 'mage/inputs/intentions'

export default function keyboardPlaterformer() {
  return {
    [JUMP]       : Keyboard.isJustDown('<space>') || Keyboard.isJustDown('<up>') || Keyboard.isJustDown('Z') || Keyboard.isJustDown('W'),
    [MOVE_LEFT]  : Keyboard.isDown('<left>') || Keyboard.isDown('Q') || Keyboard.isDown('A'),
    [MOVE_RIGHT] : Keyboard.isDown('<right>') || Keyboard.isDown('D'),
  }
}
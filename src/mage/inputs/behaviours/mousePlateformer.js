import TouchMouse from '../TouchMouse'
import {JUMP, MOVE_LEFT, MOVE_RIGHT} from 'mage/inputs/intentions'

export default function mousePlaterformer() {
  const mousePos = TouchMouse.isDown('mouse')
  return {
    [JUMP]       : mousePos && mousePos.y < window.innerHeight / 2,
    [MOVE_LEFT]  : mousePos && mousePos.x < window.innerWidth / 2,
    [MOVE_RIGHT] : mousePos && mousePos.x > window.innerWidth / 2,
  }
}
/**
 *
 * @param behaviours. A behaviour transforms some input reading (Keyboard, mouse, touch) into an aggregated intentions object.
 * @returns {{}} intentions. What the player is trying to do.
 */
export default function readIntentions(...behaviours) {
  const intents = {}

  behaviours.forEach(behaviour => {
    // get intents for one behaviour
    const behaviourIntents = behaviour()

    // merge into global intents
    Object.keys(behaviourIntents).forEach(intent => {
      if (intents[intent] != null) {
        intents[intent] = behaviourIntents[intent] || intents[intent]
      } else {
        intents[intent] = behaviourIntents[intent]
      }
    })
  })

  return intents
}

export const MOVE_LEFT = 'MOVE_LEFT'
export const MOVE_RIGHT = 'MOVE_RIGHT'
export const MOVE_TOP = 'MOVE_TOP'
export const MOVE_DOWN = 'MOVE_DOWN'
export const JUMP = 'JUMP'
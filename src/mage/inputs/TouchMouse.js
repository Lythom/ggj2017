
const TouchMouse = {
  isDown: function(key) {
    return this.data[key] != undefined ? this.inputs[key] : false
  },
  isJustDown: function(key, delta) {
    return (window.performance.now() - this.data[key] < (delta || 1000 / 60)) ? this.inputs[key] : false
  },
  isUp: function(key) {
    return this.data[key] == undefined ? this.inputs[key] : false
  },
  setDown: function(key, x, y) {
    this.data[key] = window.performance.now()
    if (typeof this.inputs[key] === 'object') {
      this.inputs[key].x = x
      this.inputs[key].y = y
    } else {
      this.inputs[key] = {x, y}
    }
  },
  setUp: function(key) {
    delete this.data[key]
    delete this.inputs[key]
  },
  data: {},
  inputs: {},
}

function onMouseUp(e) {
  if (e.changedTouches) {
    for (var i = 0; i < e.changedTouches.length; i++) {
      var touch = e.changedTouches[i];
      TouchMouse.setUp('mouse', touch.pageX, touch.pageY)
    }
  } else {
    TouchMouse.setUp('mouse', e.clientX, e.clientY)
  }
}

function onMouseDown(e) {

  if (e.changedTouches) {
    for (var i = 0; i < e.changedTouches.length; i++) {
      var touch = e.changedTouches[i];
      TouchMouse.setDown('mouse', touch.pageX, touch.pageY)
    }
  } else {
    TouchMouse.setDown('mouse', e.clientX, e.clientY)
  }
}

function onMouseMove(e) {
  if (!TouchMouse.isDown('mouse')) return
  if (e.changedTouches) {
    for (var i = 0; i < e.changedTouches.length; i++) {
      var touch = e.changedTouches[i];
      TouchMouse.setDown('mouse', touch.pageX, touch.pageY)
    }
  } else {
    TouchMouse.setDown('mouse', e.clientX, e.clientY)
  }
}

function preventScroll(e) {
  e.preventDefault()
}

export default TouchMouse

document.body.addEventListener('mousemove', onMouseMove)
document.body.addEventListener('mouseup', onMouseUp)
document.body.addEventListener('mousedown', onMouseDown)
document.body.addEventListener('touchmove', onMouseMove)
document.body.addEventListener('touchend', onMouseUp)
document.body.addEventListener('touchstart', onMouseDown)

document.body.addEventListener('touchmove', preventScroll)
document.body.addEventListener('touchmove', preventScroll)


if (module.hot) {
  module.hot.dispose(function() {
    document.body.removeEventListener('mousemove', onMouseMove)
    document.body.removeEventListener('touchmove', onMouseMove)
    document.body.removeEventListener('mouseup', onMouseUp)
    document.body.removeEventListener('touchend', onMouseUp)
    document.body.removeEventListener('touchmove', preventScroll)
  });
}

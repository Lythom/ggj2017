// Credits : ehgoodenough. https://github.com/ehgoodenough/keyb/blob/master/index.js
var vkey = require("./vkey")

const Keyboard = {
  isDown: function(key) {
    return this.data[key] != undefined
  },
  isJustDown: function(key, delta) {
    return window.performance.now() - this.data[key] < (delta || 1000 / 60)
  },
  isUp: function(key) {
    return this.data[key] == undefined
  },
  setDown: function(key) {
    this.data[key] = window.performance.now()
  },
  setUp: function(key) {
    delete this.data[key]
  },
  data: {}
}

document.addEventListener("keydown", function(event) {
  if(Keyboard.isUp(vkey[event.keyCode])) {
    Keyboard.setDown(vkey[event.keyCode])
  }
})

document.addEventListener("keyup", function(event) {
  Keyboard.setUp(vkey[event.keyCode])
})

export default Keyboard
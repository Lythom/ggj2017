import React from 'react'
import * as e2d from 'e2d'
import Sprite, {getImage} from './Sprite'
import {createTransformer} from 'mobx'
import canvasToComponent from 'mage/canvasToComponent'
import cxs from 'cxs/lite'


const cellCN = cxs({display : 'inline-block', imageRendering : 'pixelated', margin : 0, padding : 0})
const containerCN = cxs({position : 'absolute', lineHeight : 0})

function getTilesetFromGid(map, gid) {
  return map.tilesets.find(t => ((t.firstgid < gid) && (t.firstgid + t.tilecount - 1 >= gid)))
}

function getURLFromTileset(tileset) {
  return require('images/' + tileset.image.replace('images/', '').replace('.png', '') + '.png')
}

function getCoordsfromIdx(map, gid) {
  if (gid === 0) return null
  const tileset = getTilesetFromGid(map, gid)
  if (tileset == null) return {}
  const id = gid - tileset.firstgid //tmx is 1-based, not 0-based.  We need to sub 1 to get to a proper mapping.

  var x = id % tileset.columns
  var y = (id / tileset.columns)

  return {x : (parseInt(x) * parseInt(tileset.tilewidth)), y : (parseInt(y) * parseInt(tileset.tileheight)), img : getURLFromTileset(tileset)}
}

class TiledLayer extends React.Component {

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.map !== this.props.map || nextProps.layer !== this.props.layer
  }

  render() {
    const {map = {}, layer} = this.props

    const l = map.layers ? map.layers.find(l => l.name === layer) : null
    const cells = l.data
    const tilesets = map.tilesets

    const cn2 = cxs({width : map.tilewidth, height : map.tileheight})
    const containerCN2 = l == null ? '' : cxs({width : l.width * map.tilewidth, height : l.height * map.tileheight})

    return (
      <div className={containerCN + ' ' + containerCN2}>
        {!!cells && cells.map((c, i) => {
          const coords = getCoordsfromIdx(map, c)
          const tileset = !coords ? '' : cxs({
            backgroundImage    : 'url(' + coords.img + ')',
            backgroundPosition : -coords.x + 'px ' + -coords.y + 'px'
          })
          return <div className={cellCN + ' ' + cn2 + ' ' + tileset} key={i}></div>
        })}
      </div>
    )
  }
}
TiledLayer.defaultState = {}

export default TiledLayer
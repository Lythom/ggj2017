import React from 'react'
import * as e2d from 'e2d'
import {createTransformer, observable} from 'mobx'

function calcOrPx(n) {
  return typeof n === 'number' ? (~~n) + 'px' : `calc(${n} * 1px)`
}

export const getImage = createTransformer(function(entity) {
  const img = new Image()
  img.src = entity.img
  return img
})

/**
 * Canvas rendering.
 * @param entity The entity to draw
 * @returns Instruction e2d instruction variable
 */
export function e2dSprite(entity) {

  const img = getImage(entity)
  if (!img.complete) return []

  return (
    e2d.translate(entity.x + entity.width / 2, entity.y + entity.height / 2,
      e2d.scale(entity.zoom * (entity.flipX ? -1 : 1), entity.zoom * (entity.flipY ? -1 : 1),
        e2d.rotate(entity.rotate / 180 * Math.PI * (entity.flipX ? -1 : 1),
          //center the sprite
          e2d.translate(-entity.width / 2, -entity.height / 2,
            e2d.imageSmoothingEnabled(false, e2d.drawImage(img))
          )
        )
      )
    )
  )
}

/**
 * DOM rendering
 * @param entity The entity to render
 * @returns StatelessComponent Dom representation of the entity for React
 */
export default function Sprite(entity) {

  const {id, x, y, img, width, height, zoom, opacity, rotate, pivotX, pivotY, flipX, flipY, imageOffsetX, imageOffsetY, style = {}, children} = entity

  return <div id={id} className="abs entity" style={
    Object.assign({
      height,
      width,
      transform       : `translate(${calcOrPx(x)}, ${calcOrPx(y)}) ${zoom === 1 && !flipX && !flipY ? '' : ` scale(${zoom * (flipX ? -1 : 1)},${zoom * (flipY ? -1 : 1)})`}${rotate === 0 ? '' : ` rotate(${rotate * (flipX ? -1 : 1)}deg)`}`,
      opacity         : opacity,
      pointerEvents   : 'none',
      transformOrigin : `${pivotX} ${pivotY}`,
    }, style)}>
    {img && <img className="abs img" src={img} style={{zIndex : -1, transform : (imageOffsetX || imageOffsetY) ? `translate(${~~imageOffsetX}px, ${~~imageOffsetY}px)` : null}}/>}
    {children}
  </div>
}

Sprite.defaultState = {
  x            : 0, // pixels
  y            : 0, // pixels
  zoom         : 1,
  opacity      : 1,
  rotate       : 0,
  pivotX       : '50%',
  pivotY       : '50%',
  flipX        : false,
  flipY        : false,
  imageOffsetX : null,
  imageOffsetY : null,
  img          : null,
  width        : 64,
  height       : 64,
}
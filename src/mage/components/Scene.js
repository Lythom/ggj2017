import React from 'react'

export default function Scene({ style, className, children }) {
  return <div className={`abs scene  ${className ? className : ''}`} style={style}>
    {children}
  </div>
}

Scene.defaultState = {}
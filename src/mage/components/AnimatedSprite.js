import React from 'react'
import * as e2d from 'e2d'
import Sprite, {getImage} from './Sprite'

const EMPTY_ANIM = {frames: [], expositions: 1}

const frames = {}
function createFrame(entity, ix) {
  const offscreenCanvas = document.createElement("canvas")
  offscreenCanvas.width = entity.width
  offscreenCanvas.height = entity.height
  const ctx = offscreenCanvas.getContext("2d")
  offscreenCanvas.addEventListener('load', () => {
    e2d.render(
      e2d.imageSmoothingEnabled(false, e2d.drawImage(getImage(entity), ix, 0, entity.width, entity.height, 0, 0, entity.width, entity.height)),
      ctx
    )
  })
  return ctx
}

export function e2dAnimatedSprite(entity) {
  const ix = entity.currentFrame * entity.width

  const img = getImage(entity)
  if (!img.complete) return []

  if (frames[entity.img] == null) frames[entity.img] = {}
  if (frames[entity.img][ix] == null) frames[entity.img][ix] = e2d.imageSmoothingEnabled(false, e2d.drawImage(img, ix, 0, entity.width, entity.height, 0, 0, entity.width, entity.height))
  const frame = frames[entity.img][ix]

  return (
    e2d.translate(parseInt(entity.x + entity.width / 2), parseInt(entity.y + entity.height / 2),
      e2d.scale(entity.zoom * (entity.flipX ? -1 : 1), entity.zoom * (entity.flipY ? -1 : 1),
        e2d.rotate(entity.rotate / 180 * Math.PI * (entity.flipX ? -1 : 1),
          //center the sprite
          e2d.translate(parseInt(-entity.width / 2), parseInt(-entity.height / 2),
            frame
          )
        )
      )
    )
  )
}

/**
 * DOM rendering for a spritesheet animation.
 * use the {step} function on update to step the animation, and the {transitionToAnimation} function to change animation with optional transition steps.
 * @param entity
 * @returns {XML}
 * @constructor
 */
export default function AnimatedSprite(entity) {

  // keep props specific to Sprite
  const {x, y, img, width, height, zoom, opacity, rotate, pivotX, pivotY, flipX, flipY, children} = entity
  const spriteProps = {x, y, img, width, height, zoom, opacity, rotate, pivotX, pivotY, flipX, flipY}

  return <Sprite {...spriteProps} imageOffsetX={-entity.currentFrame * entity.width}
                 style={Object.assign({overflow: 'hidden'}, entity.style)}>
    {children}
  </Sprite>
}

AnimatedSprite.defaultState = Object.assign({
  currentFrame: 0,
}, Sprite.defaultState)


// BEHAVIOURS


export function transitionToAnimation(state, animation) {
  if (animation == null || !(animation in state.animations)) throw new Error(`Can' set a non existing animation. Please declare a "${animation}" key in animations.`)

  // if the animation change
  if (state.animation !== animation) {

    // We alreay checked if the animation exists, assume it's a valid animation
    const nextAnim = state.animations[animation]

    // start the next animation from 0
    state.framesElapsed = 0
    state.currentStep = 0
    state.currentFrame = getCurrentFrame(state) || 0

    // if the next anim have a transition from the current one, trigger it
    if (nextAnim[state.animation]) {
      state.transitionFrom = state.animation
    }

    state.animation = animation
  }
}

export function step(state, progress) {
  // init vars
  if (state.framesElapsed == null) state.framesElapsed = 0
  if (state.currentStep == null) state.currentStep = 0

  // nullify non-existing transitions
  if (state.transitionFrom != null && state.animations[state.transitionFrom] == null) state.transitionFrom = null

  const anim = getCurrentAnim(state)
  let expo = getCurrentExposition(state)

  state.currentFrame = getCurrentFrame(state) || 0

  // we should switch
  if (state.framesElapsed >= expo && anim != null) {
    const animIsOver = state.currentStep + 1 >= anim.frames.length

    // update progression state
    state.currentStep = (animIsOver) ? 0 : state.currentStep + 1
    state.framesElapsed = 0

    // stop transitionning if a transition was occuring
    if (animIsOver) state.transitionFrom = null

  } else {
    state.framesElapsed += progress
  }
}

function getCurrentFrame(state) {
  const anim = getCurrentAnim(state)
  if (anim.frames.length > state.currentStep) {
    return anim.frames[state.currentStep]
  }
  return null
}

function getCurrentExposition(state) {
  const anim = getCurrentAnim(state)
  if (typeof anim.expositions === 'number') return anim.expositions
  if (anim.expositions.length > state.currentStep) {
    return anim.expositions[state.currentStep]
  }
  return 1
}

function getCurrentAnim(state) {
  if (state.animations == null || !state.animations[state.animation]) return EMPTY_ANIM
  const currAnim = state.animations[state.animation]
  const inTransition = state.transitionFrom != null && currAnim[state.transitionFrom] != null
  return (inTransition ? currAnim[state.transitionFrom] : currAnim)
}

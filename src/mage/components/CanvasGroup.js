import React, {Children} from 'react'
import * as e2d from '../../e2d'

export default class CanvasGroup extends React.Component {

  static defaultProps = {
    rotate : 0,
    zoom   : 1,
    x      : 0,
    y      : 0,
  }

  attachToCanvas(id, func) {
    function decoratedInstructionFunc() {
      return e2d.rotate(this.props.rotate / 180 * Math.PI,
        e2d.scale(this.props.zoom,
          //center the sprite
          e2d.translate(this.props.x, this.props.y,
            func()
          )
        )
      )
    }
    this.props.attachToCanvas(id, decoratedInstructionFunc.bind(this))
  }

  render() {
    const {attachToCanvas, detachFromCanvas, children} = this.props

    const childrenWithProps = React.Children.map(this.props.children,
      (child) => React.cloneElement(child, {
        attachToCanvas : this.attachToCanvas.bind(this),
        detachFromCanvas   : this.props.detachFromCanvas,
      })
    );
    return <div>{childrenWithProps}</div>
  }
}
import React from 'react'
import * as e2d from 'e2d'
import Sprite, {getImage} from './Sprite'
import {createTransformer} from 'mobx'
import canvasToComponent from 'mage/canvasToComponent'
import cxs from 'cxs/lite'

let hexagonShape = e2d.createRegularPolygon(10, [0, 0], 7);
let hexagonI = [e2d.path( //always wrap drawn paths with e2d.path
  hexagonShape.map(e2d.moveToLineTo) //map each point to an instruction
), e2d.fillStyle('red', e2d.fill())]

function calcOrPx(n) {
  return typeof n === 'number' ? (~~n) + 'px' : `calc(${n} * 1px)`
}

const imgs = {}
export const getImageFromSrc = function(src) {
  if (imgs[src] != null) return imgs[src]
  const img = new Image()
  img.src = src
  imgs[src] = img
  return img
}

function getTilesetFromGid(map, gid) {
  return map.tilesets.find(t => ((t.firstgid < gid) && (t.firstgid + t.tilecount - 1 >= gid)))
}

function getURLFromTileset(tileset) {
  return require('images/' + tileset.image.replace('images/', '').replace('.png', '') + '.png')
}

function getCoordsfromIdx(map, gid) {
  if (gid === 0) return null
  const tileset = getTilesetFromGid(map, gid)
  if (tileset == null) return {}
  const id = gid - tileset.firstgid //tmx is 1-based, not 0-based.  We need to sub 1 to get to a proper mapping.

  var x = id % tileset.columns
  var y = (id / tileset.columns)

  return {x : (parseInt(x) * parseInt(tileset.tilewidth)), y : (parseInt(y) * parseInt(tileset.tileheight)), img : getImageFromSrc(getURLFromTileset(tileset))}
}

class CanvasTiledLayer extends React.Component {

  constructor() {
    super()
    this.state = {canvas : null}
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.map !== this.props.map
  }

  componentDidMount() {
    this.redraw()
  }

  componentDidUpdate(prevProps, prevState) {
    this.redraw()
  }

  registerCanvas(canvas) {
    this.setState({canvas}, this.redraw)
  }

  redraw() {
    console.log('try redrawing canvas')
    if (this.state.canvas == null) return
    console.log('redrawing canvas')
    const ctx = this.state.canvas.getContext("2d")

    const {map, layer} = this.props
    const l = map.layers ? map.layers.find(l => l.name === layer) : null
    if (l == null) throw new Error(`Layer ${layer} was not found in the map. Found layers : ${map.layers.join(', ')}`)
    const cells = l.data

    const getCoordsImg = getCoordsfromIdx.bind(null, map)
    const coords = cells.map(getCoordsImg)
    const imgReady = Promise.all(
      coords
        .filter(c => c != null)
        .map(c => c.img)
        .filter(img => !img.complete)
        .map(img => new Promise((resolve, reject) => {
            img.addEventListener('load', () => resolve(img))
            img.addEventListener('error', () => reject(img))
          }
        ))
    )
    imgReady.then(() => {
      const instructions = coords.map((cellData, idx) => cellData == null ? null : (
        e2d.imageSmoothingEnabled(false,
          e2d.drawImage(
            cellData.img,
            cellData.x, cellData.y, map.tilewidth, map.tileheight,
            (idx % l.width) * map.tilewidth, parseInt(idx / l.width) * map.tileheight, map.tilewidth, map.tileheight
          )
        )
      ))
        .filter(i => i !== null)

      e2d.render(
        e2d.clearRect(this.state.canvas.width, this.state.canvas.height),
        instructions,
        ctx
      )
    })
  }

  render() {
    const {map, layer} = this.props
    if (map == null) return null
    const l = map.layers ? map.layers.find(l => l.name === layer) : null
    if (l == null) throw new Error(`Layer ${layer} was not found in the map. Found layers : ${map.layers.join(', ')}`)

    const width = l.width * map.tilewidth
    const height = l.height * map.tileheight

    return (
      <canvas className="abs" ref={this.registerCanvas.bind(this)} width={width} height={height}/>
    )
  }
}
CanvasTiledLayer.defaultState = {}

export default CanvasTiledLayer
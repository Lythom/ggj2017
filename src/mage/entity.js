import React from 'react'
import {inject, observer} from 'mobx-react'
import {entities} from 'mage/stores/entities'

export const defaultState = {
  x: 0, // pixels
  y: 0, // pixels
  accelerationX: 0, // pixels / second / second (framerate independant)
  accelerationY: 0, // pixels / second / second (framerate independant)
  velocityX: 0, // pixels / second (framerate independant)
  velocityY: 0, // pixels / second (framerate independant)
  maxVelocityX: 1000, // pixels / second (framerate independant)
  angularVelocity: 0, // degrees / second
  angularAcceleration: 0, // degrees / second / second
  width: 64,
  height: 64,
  freeze: false,
  visibleInstances: 0,
}

export const mapIdsToEntities = func => (...params) => {
  if (!entities) return
  return func(...params.map(p => entities.has(p) ? entities.get(p) : p))
}

export const updatePosition = mapIdsToEntities(function (entity, elapsed) {
  if (typeof entity !== 'object' || entity.freeze) return
  const elapsedInSec = elapsed / 1000
  entity.velocityX = computeVelocity(entity.velocityX, entity.accelerationX, entity.maxVelocityX, elapsedInSec)
  entity.x = entity.x + entity.velocityX * elapsedInSec
  entity.velocityY = computeVelocity(entity.velocityY, entity.accelerationY, entity.maxVelocityY, elapsedInSec)
  entity.y = entity.y + entity.velocityY * elapsedInSec
  entity.angularVelocity = computeVelocity(entity.angularVelocity, entity.angularAcceleration, 60 * 360, elapsedInSec)
  entity.rotate += entity.angularVelocity * elapsedInSec

})

/**
 *
 * @param velocity
 * @param acceleration
 * @param maxVelocity
 * @param elapsed
 * @returns {*}
 */
function computeVelocity(velocity, acceleration, maxVelocity, elapsed) {
  if (acceleration != 0) {
    velocity += acceleration * elapsed;
  }
  if ((velocity != 0) && (maxVelocity != 0)) {
    if (velocity > maxVelocity) {
      velocity = maxVelocity;
    }
    else if (velocity < -maxVelocity) {
      velocity = -maxVelocity;
    }
  }
  return velocity;
}

const selectParentOrEntitiesStore = (stores, ownProps) => ({entities: ownProps.parent && ownProps.parent.instances ? ownProps.parent.instances : stores.entities})

/**
 * @name entity
 * Allows any React Component to declare an entity on mount (a React component bound to the entities store) from a stateless React Component.
 * An entity with the same id can have several representations, but only the first declaration will initiate the state with it's factoryInitialState.
 * When all entity declararers are remove from the React tree, the entity will have it's freeze property set to true.
 * Default behaviours don't update frozen entities.
 * @param {React.Component} Comp React Component the will receive the entity as props.
 * @param factoryInitialState data to use as default initial state for any entity declared with this component.
 *        To be chosen depending on it's expected behaviour. (ex: AnimatedSprite.initialState)
 * @returns {React.Component} A new component that accepts an "id" props to declare a new entity, and an "initialState" property to init instance specific values.
 */
export default function entity(Comp, ...factoryInitialState) {
  const OComp = observer(Comp)
  return inject(selectParentOrEntitiesStore)(observer(class Entity extends React.Component {

      register(entities, id) {
        if (id == null) throw new Error("The entity must have an id. Please provide an id prop to all yours entities.")
        if (!entities.has(id)) {
          entities.set(id, Object.assign({}, defaultState, ...factoryInitialState, this.props.initialState, {
            id,
            visibleInstances: 1
          }))
        } else {
          const e = entities.get(id)
          e.visibleInstances += 1
          e.freeze = false
        }
      }

      componentDidMount() {
        this.register(this.props.entities, this.props.id)
      }

      componentWillReceiveProps(nextProps) {
        if (nextProps.id != this.props.id) {
          this.register(nextProps.entities, nextProps.id)
        }
      }

      componentWillUnmount() {
        const {entities, id} = this.props
        if (entities.has(id)) {
          const e = entities.get(id)
          e.visibleInstances -= 1
          if (e.visibleInstances == 0) {
            e.freeze = true
          }
        }
      }

      render() {
        const {entities, id, parent, children, ...otherProps} = this.props
        const e = entities.get(id)
        if (e == null) return null
        return <OComp entity={e} id={id} {...otherProps}>
          {children}
        </OComp>
      }

    }
  ))
}

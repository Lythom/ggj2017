import React from 'react'
import entity, {mapIdsToEntities} from '../entity'

export const followPath = mapIdsToEntities((entity, path) => {
  if (typeof entity !== 'object') return
  entity.x += 1
})

/**
 *
 */
export default entity(function Path({entity, style, children}) {

  const {x, y, width, height, points} = entity
  const path = points.map((p, i) => {
    if (i == 0) return `M${p.x} ${p.y}`
    return `L ${p.x} ${p.y}`
  })
  path.push('Z')

  return <svg ref={i => entity.ref = i} width={width} height={height}>
{/*    <defs>
      <filter id="filter" filterUnits="userSpaceOnUse" x="-5" y="-5" height="2000" width="2000">
        <feTurbulence baseFrequency="0.2" numOctaves="3" type="fractalNoise"/>
        <feDisplacementMap scale="8" xChannelSelector="R" in="SourceGraphic"/>
      </filter>
    </defs>*/}
    <path d={path.join(' ')} fill="transparent" stroke="black" strokeWidth="5"/>
  </svg>

}, {
  x      : 0,
  y      : 0,
  width  : 64,
  height : 64,
})

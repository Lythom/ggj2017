import React from 'react'
import entity, {mapIdsToEntities} from 'mage/entity'
import {observable, createTransformer} from 'mobx'

export const track = mapIdsToEntities((cameraEntity, trackedEntity, speed = 1, offsetX = 0, offsetY = 0, limits = {
  top: -100,
  left: -100,
  right: 3000,
  bottom: 1000
}) => {
  if (typeof cameraEntity !== 'object' || typeof trackedEntity !== 'object' || cameraEntity.width === 0) return
  const deltaX = trackedEntity.x + trackedEntity.width / 2 + offsetX - cameraEntity.x - cameraEntity.width / 2
  const deltaY = trackedEntity.y + trackedEntity.height / 2 + offsetY - cameraEntity.y - cameraEntity.height / 2

  cameraEntity.x += deltaX / (20 / speed)
  if (cameraEntity.x < limits.left) cameraEntity.x = limits.left
  if (cameraEntity.x + cameraEntity.width > limits.right) cameraEntity.x = limits.right - cameraEntity.width
  cameraEntity.y += deltaY / (20 / speed)
  if (cameraEntity.y < limits.top) cameraEntity.y = limits.top
  if (cameraEntity.y + cameraEntity.height > limits.bottom) cameraEntity.y = limits.bottom - cameraEntity.height


})

const getRegisterCameraSize = createTransformer(cameraEntity => function registerCameraSize(cameraEntity, divReference) {
  console.log('registering camera')
  if (divReference == null) return
  cameraEntity.width = divReference.offsetWidth
  cameraEntity.height = divReference.offsetHeight
})

/**
 *
 */
function Camera({entity, className, style, children}) {
  const {x, y, zoom, rotate} = entity
  const scale = entity.autoScaleHeight != null ? entity.zoom * entity.height / entity.autoScaleHeight  : zoom
  const transform = `
      rotate(${rotate}deg)
      scale(${scale})
      translate(${-x}px, ${-y}px)
    `

  const rcs = getRegisterCameraSize(entity)

  return <div style={Object.assign({overflow: 'hidden'}, style)}>
    <div ref={rcs} className={`abs container ${className ? className : ''}`} style={{transform}}>
      {children}
    </div>
  </div>
}

Camera.defaultState = {
  x: 0,
  y: 0,
  zoom: 1,
  rotate: 0,
  width: 0,
  height: 0,
  instructions: observable.map({}),
  autoScaleHeight: null,
}

export default entity(Camera, Camera.defaultState)
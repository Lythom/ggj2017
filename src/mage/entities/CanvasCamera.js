import React, {Children} from 'react'
import entity, {mapIdsToEntities} from 'mage/entity'
import * as e2d from 'e2d'

import {autorun, observable, computed} from 'mobx'
import {entities} from 'mage/stores/entities'

function registerCameraSize(cameraEntity, divReference) {
  if (divReference == null) return
  cameraEntity.width = divReference.offsetWidth
  cameraEntity.height = divReference.offsetHeight
}

function registerCtx(cameraEntity, canvasReference) {
  if (canvasReference == null) return
  cameraEntity.ctx = canvasReference.getContext('2d')
}

export function draw(entity, entities) {

  const instructions = entity.instructions.values().map(getInstr => getInstr())
  const scale = entity.autoScaleHeight != null ? entity.zoom * entity.height / entity.autoScaleHeight  : entity.zoom

  e2d.render(
    e2d.clearRect(entity.width, entity.height),
    e2d.translate(entity.width / 2, entity.height / 2,
      e2d.rotate(entity.rotate / 180 * Math.PI,
        e2d.scale(scale,
          //center the sprite
          e2d.translate(-entity.x - entity.width / 2, -entity.y - entity.height / 2,
            instructions
          )
        )
      )),
    entity.ctx
  )
}

class CanvasCamera extends React.Component {

  componentDidMount() {
    this.disposeAutorun = autorun(() => draw(this.props.entity, entities))
  }

  componentWillUnmount() {
    if (this.disposeAutorun) this.disposeAutorun()
  }

  attachToCanvas(id, func) {
    this.props.entity.instructions.set(id, func)
  }

  detachFromCanvas(id) {
    this.props.entity.instructions.delete(id)
  }

  render() {
    const {entity, style, children} = this.props
    const {x, y, zoom, width, height, autoScaleHeight, rotate, ctx} = entity

    if (typeof children !== 'function') throw new Error('CanvasCamera children must be a function. This function is provided a "attach" parameter for CanvasComponents to be able to declare their drawing on this canvas.\nex: <CanvasCamera id="camera">{attach => <ChildCanvasComponent {...attach} />}</CanvasCamera>')

    return <div ref={registerCameraSize.bind(null, entity)} className="abs container" style={style}>
      <canvas ref={registerCtx.bind(null, entity)} width={width} height={height}/>
      {children({
        attachToCanvas : this.attachToCanvas.bind(this),
        detachFromCanvas   : this.detachFromCanvas.bind(this),
      })}
    </div>
  }
}

CanvasCamera.defaultState = {
  x            : 0,
  y            : 0,
  zoom         : 1,
  rotate       : 0,
  width        : 0,
  height       : 0,
  instructions : observable.map({}),
}

/**
 *
 */
export default entity(CanvasCamera, CanvasCamera.defaultState)

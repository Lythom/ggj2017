import {observable} from 'mobx'

export const entities = observable.map({})

export function add(e) {
  return entities.set(e.id, e)
}
export function remove(id) {
  return entities.delete(id)
}
export function get(id) {
  return entities.get(id)
}
import React from 'react'
import {observable} from 'mobx'
import entity, {mapIdsToEntities} from './entity'

export const spawn = mapIdsToEntities(function(spawnerEntity, spawnRate, initialProperty) {
  spawnerEntity.spawnAccumulator += spawnRate
  while (spawnerEntity.spawnAccumulator > 1) {
    spawnerEntity.inits.set(spawnerEntity.nextId, initialProperty)
    spawnerEntity.nextId += 1
    spawnerEntity.spawnAccumulator -= 1
  }
})

/**
 *
 * @param spawnerEntity
 * @param spawnChance
 * @param initialProperty
 */
export const randomlySpawn = mapIdsToEntities(function(spawnerEntity, spawnChance, initialProperty) {
  if (Math.random() < spawnChance) {
    spawnerEntity.inits.set(spawnerEntity.nextId, initialProperty)
    spawnerEntity.nextId += 1
  }
})

export const bulkDestroy = mapIdsToEntities(function(spawnerEntity, destroyCondition) {
  const toBeDestroyed = spawnerEntity.instances.values().filter(destroyCondition).map(tbd => tbd.id)
  toBeDestroyed.forEach(id => {
    spawnerEntity.inits.delete(id)
    spawnerEntity.instances.delete(id)
  })
})

export const bulkUpdate = mapIdsToEntities(function(spawnerEntity, updateFct) {
  spawnerEntity.instances.values().forEach(updateFct)
})

/**
 * Create a spawner entity from any entity.
 *
 * @param ChildEntityComponent
 * @param factoryInitialState
 * @returns {React.Component}
 */
function spawner(ChildEntityComponent, ...factoryInitialState) {
  return entity(({entity, children, id, ...otherProps}) => <div>
    {entity.inits.entries().map(init => {
      return <ChildEntityComponent key={init[0]} id={init[0]} parent={entity} initialState={init[1]} {...otherProps}>
        {children}
      </ChildEntityComponent>
    })}
  </div>, defaultState, ...factoryInitialState)
}

export const defaultState = {
  inits            : observable.map({}),
  instances        : observable.map({}),
  nextId           : 0,
  spawnAccumulator : 0,
}

export default spawner
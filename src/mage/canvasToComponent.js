import React from 'react'

/**
 * Canvas rendering function that returns draw instructions for an entity.
 *
 * @callback getInstruction
 * @param {object} entity The entity to draw
 * @return Instruction e2d draw instructions
 */

let idGen = 0

/**
 * Transforms any e2d rendering function into React Component able to declare an entity on mount (a React component bound to the entities store).
 * Those specific Canvas Components will only be rendered when placed as children of a CanvasCamera or a CanvasGroup
 * @param {getInstruction} getInstruction e2d rendering function
 */
export default function canvasToComponent(getInstruction) {

  class CanvasComponent extends React.Component {

    static get propTypes() {
      return {
        id : React.PropTypes.string
      }
    }

    getInstr() {
      return getInstruction(this.props.entity ? Object.assign({}, this.props, this.props.entity) : this.props)
    }

    componentDidMount() {
      if (this.props.attachToCanvas == null) throw new Error(`canvasToComponent must be attached to a canvas. Ex: <CanvasCamera id="camera">{attach => <${getInstruction.name} {...attach} />}</CanvasCamera>`)
      this.id = this.props.id || ("unregisteredCanvasElement" + (idGen++))
      this.props.attachToCanvas(this.id, this.getInstr.bind(this))
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.id != null && this.id !== nextProps.id) {

        this.props.detachFromCanvas(this.id)
        this.props.attachToCanvas(nextProps.id, this.getInstr.bind(this))
        this.id = nextProps.id
      }
    }

    componentWillUnmount() {
      this.props.detachFromCanvas(this.id)
    }

    render() {
      return null
    }
  }

  return CanvasComponent
}
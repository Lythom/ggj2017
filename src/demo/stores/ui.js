import {observable} from 'mobx'

export const ui = observable({
  currentScene : 'WhiteScene',
  mouseX       : 0,
  mouseY       : 0,
  fps          : 0,
  mode         : 'canvas'
})

export function goto(scene) {
  ui.currentScene = scene
}

export function changeMode(scene) {
  if (ui.mode === 'dom') {
    ui.mode = 'canvas'
  }
  else {
    ui.mode = 'dom'
  }
}

export function setMousePosition(x, y) {
  ui.mouseX = x
  ui.mouseY = y
}
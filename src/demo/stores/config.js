import {observable} from 'mobx'

export const config = observable({
  gameTitle : 'Molten Adamantium',
  scaleMode : 'responsive', // responsive, fluidZoom, steppedZoom
})

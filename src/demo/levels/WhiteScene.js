import React from 'react'
import p2 from 'p2'
import Scene from 'mage/components/Scene'
import cxs from 'cxs/lite'

import AnimatedTestCharEntity, {CanvasAnimatedTestCharEntity} from 'src/demo/entities/AnimatedTestCharEntity'
import PlaceholderEntity, {CanvasPlaceholderEntity} from 'src/demo/entities/PlaceholderEntity'

import entity from 'mage/entity'
import spawner from 'mage/spawner'
import CanvasCamera from 'mage/entities/CanvasCamera'
import Sprite from 'mage/components/Sprite'
import Camera from 'mage/entities/Camera'

import Table from 'src/demo/entities/Table'
import Tablecloth from 'src/demo/entities/Tablecloth'


export default function WhiteScene({children, mode}) {
  return <Scene>
    <Camera id="camera" initialState={{autoScaleHeight: 1080}}>
      <Sprite img={require('images/BMC-Table.png')}/>
      <Table id="table" style={{backgroundColor: 'black'}}/>
      <Tablecloth id="tablecloth" />
    </Camera>
    <CanvasCamera id="camera">
      {attach => <div>
        <CanvasPlaceholderEntity id="test" {...attach}/>
      </div>}
    </CanvasCamera>
  </Scene>
}
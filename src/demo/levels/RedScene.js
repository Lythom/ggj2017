import React from 'react'
import Scene from '../../mage/components/Scene'
import Camera from '../../mage/entities/Camera'
import CanvasCamera from 'mage/entities/CanvasCamera'
import CanvasGroup from '../../mage/components/CanvasGroup'


import PlaceholderEntity, {CanvasPlaceholderEntity} from 'src/demo/entities/PlaceholderEntity'
import AnimatedTestChar, {CanvasAnimatedTestCharEntity} from 'src/demo/entities/AnimatedTestCharEntity'
import Path from 'src/mage/entities/Path'

const cornerCameraStyle = {
  width     : '50%',
  height    : '50%',
  position  : 'relative',
  border    : '1px solid brown',
  boxSizing : 'border-box'
}


export default class RedScene extends React.Component {

  render() {
    const {children, mode} = this.props

    const canvasSceneContent = attach => <CanvasGroup {...attach}>
      {/*<div className="abs dividedGridCell10" style={{width : '300%', height : '300%', left : '-100%', top : '-100%'}}></div>
      <div className="abs dividedGridCell100" style={{width : '300%', height : '300%', left : '-100%', top : '-100%'}}></div>*/}
      <CanvasAnimatedTestCharEntity id="test2" {...attach} initialState={{
        x         : 50,
        y         : 150,
        animation : 'run',
      }}/>
      <CanvasPlaceholderEntity {...attach} id="freemovingtest"/>
      <CanvasPlaceholderEntity {...attach} id="test"/>
      {/*<Path id="thePath" initialState={{ x: 0, y:0, width: 1000, height: 1000, points:[{x:0,y:0}, {x:560,y:50}, {x:1000,y:1000}, {x:50,y:750}, {x:125,y:500}]}} />*/}
    </CanvasGroup>

    const sceneContent = <div>
      <div className="abs dividedGridCell10" style={{width : '300%', height : '300%', left : '-100%', top : '-100%'}}></div>
      <div className="abs dividedGridCell100" style={{width : '300%', height : '300%', left : '-100%', top : '-100%'}}></div>
      <AnimatedTestChar id="test2" initialState={{
        x         : 50,
        y         : 150,
        animation : 'run',
      }}/>
      <PlaceholderEntity id="freemovingtest"/>
      <PlaceholderEntity id="test"/>
    </div>

    return <Scene style={{backgroundColor : '#FFEAED'}}>
      <span className="abs">Level Red</span>
      {mode === 'dom' && <div className="abs scene" style={{display : 'flex', flexWrap : 'wrap'}}>
        <Camera id="camera" style={cornerCameraStyle} initialState={{x : 0, y : 0, zoom : 1, rotate : 0}}>
          {sceneContent}
        </Camera>
        <Camera id="camera2" style={cornerCameraStyle} initialState={{x : -110, y : -110, zoom : 2, rotate : 0}}>
          {sceneContent}
        </Camera>
        <Camera id="camera3" style={cornerCameraStyle} initialState={{x : -250, y : -20, zoom : 1, rotate : 45}}>
          {sceneContent}
        </Camera>
        <Camera id="camera4" style={cornerCameraStyle} initialState={{x : -400, y : -300, zoom : 0.5, rotate : 0}}>
          {sceneContent}
        </Camera>
      </div>}
      {mode === 'canvas' && <div className="abs scene" style={{display : 'flex', flexWrap : 'wrap'}}>
        <CanvasCamera id="camera" style={cornerCameraStyle} initialState={{x : 0, y : 0, zoom : 1, rotate : 0}}>
          {canvasSceneContent}
        </CanvasCamera>
        <CanvasCamera id="camera2" style={cornerCameraStyle} initialState={{x : -110, y : -110, zoom : 2, rotate : 0}}>
          {canvasSceneContent}
        </CanvasCamera>
        <CanvasCamera id="camera3" style={cornerCameraStyle} initialState={{x : -250, y : -20, zoom : 1, rotate : 45}}>
          {canvasSceneContent}
        </CanvasCamera>
        <CanvasCamera id="camera4" style={cornerCameraStyle} initialState={{x : -400, y : -300, zoom : 0.5, rotate : 0}}>
          {canvasSceneContent}
        </CanvasCamera>
      </div>}
      <div className="abs scene" style={{display : 'flex', flexWrap : 'wrap', pointerEvents : 'none'}}>
        <div style={cornerCameraStyle}>
          <div style={{textAlign : 'center'}}>Camera 1 : Normal</div>
        </div>
        <div style={cornerCameraStyle}>
          <div style={{textAlign : 'center'}}>Camera 2 : Zoom x2, x -110, y -110</div>
        </div>
        <div style={cornerCameraStyle}>
          <div style={{textAlign : 'center'}}>Camera 3 : Rotate 45deg, x -250, y -20</div>
        </div>
        <div style={cornerCameraStyle}>
          <div style={{textAlign : 'center'}}>Camera 4 : Moving camera rotation, x -400, y -300, zoom 0.5</div>
        </div>
      </div>

    </Scene>
  }
}
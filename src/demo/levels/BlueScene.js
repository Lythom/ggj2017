import React from 'react'
import {inject, observer} from 'mobx-react'

import Scene from '../../mage/components/Scene'

import PlaceholderEntity from 'src/demo/entities/PlaceholderEntity'

export default () => {

  return <Scene style={{backgroundColor : '#EFECFF'}}>
    <span>Level Blue - Using CSS var to place an element relatively to the mouse. Ex: during drag and drop</span>
    <PlaceholderEntity id="mouseFollower" style={{transform : 'translate(-32px, -64px)'}}/>
  </Scene>
}
import React from 'react'

import entity from 'mage/entity'
import Sprite from '../../mage/components/Sprite'

export default entity(({entity, style, children}) => {
    return <Sprite {...entity} style={{overflow: 'hidden'}}/>
  },
  Sprite.defaultState,
  {
    img    : require('images/square-placeholder.png'),
    width  : 20,
    height : 20,
  }
)

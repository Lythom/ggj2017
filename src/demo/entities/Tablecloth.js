import React from 'react'
import p2 from 'p2'
import {observable} from 'mobx'

const WORLD_TO_PX = 10
const ballRadius = 1

import entity, {mapIdsToEntities} from 'mage/entity'
import spawner, {spawn} from 'mage/spawner'
import Sprite from '../../mage/components/Sprite'
import TableclothPiece from './TableclothPiece'

export const initBodies = mapIdsToEntities(function initBodies(entity, world) {
  if (typeof entity !== 'object' || entity.inits.keys().length > 0) return
  let lastBody = null
  for (let i = 0; i < 100; i++) {
    var p = new p2.Body({
      mass: i == 0 || i == 94 ? 0 : 1,
      position: [0 + i * 2, 80],
    });
    p.addShape(new p2.Circle({radius: ballRadius}))
    world.addBody(p)

    if (lastBody) {
      var dist = Math.abs(p.position[0] - lastBody.position[0]),
        c = new p2.DistanceConstraint(p, lastBody, {
          distance: dist + 0.1
        })
      world.addConstraint(c)
    }
    lastBody = p
    spawn(entity, 1, Object.assign({bodyId: p.id}))
  }
})

export default spawner(TableclothPiece)

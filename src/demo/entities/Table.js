import React from 'react'
import p2 from 'p2'
import {observable} from 'mobx'

const WORLD_TO_PX = 10

import entity, {mapIdsToEntities} from 'mage/entity'
import spawner from 'mage/spawner'
import Sprite, {e2dSprite} from '../../mage/components/Sprite'
import canvasToComponent from 'mage/canvasToComponent'

export const initTableBody = mapIdsToEntities(function initBodies(entity, world) {
  if (typeof entity !== 'object' || entity.bodyId != null) return
  var p = new p2.Body({
    mass: 0,
    position: [192/2 - 1, 100]
  })
  p.addShape(new p2.Box({width: entity.width / WORLD_TO_PX, height: entity.height / WORLD_TO_PX}))
  world.addBody(p)
  entity.bodyId = p.id
})

const initialState = Object.assign({},
  Sprite.defaultState,
  {
    width: 1500,
    height: 165,
    x: 170,
    y: 1080 - 165,
    opacity: 0.1,
  })

export default entity(({entity, style}) => <Sprite {...entity} style={style}/>, initialState)

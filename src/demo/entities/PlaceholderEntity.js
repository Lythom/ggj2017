import React from 'react'

import entity, {mapIdsToEntities} from 'mage/entity'
import Sprite, {e2dSprite} from '../../mage/components/Sprite'
import canvasToComponent from 'mage/canvasToComponent'

export const followPath = mapIdsToEntities((entity, path) => {
  if (typeof entity !== 'object') return
  entity.x += 1
})

let i = 0
export const turnAround = mapIdsToEntities((entity, delta) => {
  if (typeof entity !== 'object') return
  entity.x = ~~(Math.cos((i) / 500) * 50 + 100)
  entity.y = ~~(Math.sin((i) / 500) * 50 + 100)
  i = (i + delta) % (360 * 500)
})

const initialState = Object.assign({}, Sprite.defaultState,
  {
    img    : require('images/square-placeholder.png'),
    width  : 64,
    height : 64,
    x      : 50,
    y      : 50,
    rotate : 0,
  })

export default entity(({entity, style, children}) => {
    return <div className="abs" style={style}>
      <Sprite {...entity}>
        <div style={{color : 'white', textShadow : '1px 1px 1px black, -1px -1px 1px black', padding : 16}}>{entity.id} {children}</div>
      </Sprite>
    </div>
  },
  initialState
)

export const CanvasPlaceholderEntity = entity(canvasToComponent(e2dSprite), initialState)
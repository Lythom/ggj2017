import React from 'react'
import {observable} from 'mobx'

import entity from 'mage/entity'
import canvasToComponent from 'mage/canvasToComponent'
import AnimatedSprite, {e2dAnimatedSprite} from '../../mage/components/AnimatedSprite'

const initialState = Object.assign({},
  AnimatedSprite.defaultState,
  {
    img           : require('images/Test-sprite00000_72x81_11x1.png'),
    width         : 74,
    height        : 81,
    x             : 50,
    y             : 250,
    loop          : true,
    accelerationY : 600,// simulate gravity
    animation     : 'jump',
    animations    : observable.ref({
      iddle : {
        frames      : [7],
        expositions : [1],
        run         : {
          frames      : [9, 10],
          expositions : 2,
        },
      },
      run   : {
        frames      : [0, 1, 2, 3, 4, 5, 6],
        expositions : 2,
        iddle       : {
          frames      : [8],
          expositions : 2,
        }
      },
      jump  : {
        frames      : [2],
        expositions : 2,
        iddle       : {
          frames      : [8],
          expositions : 2,
        },
        run         : {
          frames      : [8],
          expositions : 2,
        },
      },
      swap  : {
        frames      : [9],
        expositions : 2,
      }
    }),
  })

export default entity(
  ({entity, style, children}) => {
    return <div className="abs" style={style}>
      <AnimatedSprite {...entity} >
        {children}
      </AnimatedSprite>
    </div>
  },
  initialState
)

export const CanvasAnimatedTestCharEntity = entity(canvasToComponent(e2dAnimatedSprite), initialState)
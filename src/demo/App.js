import './App.css'

import React from 'react'
import {observer, inject} from "mobx-react"
import cxs from 'cxs/lite'
import {goto, changeMode} from 'src/demo/stores/ui'

import Scene from '../mage/components/Scene'

const App = inject('ui')(observer(({ui = {}}) => {

    const Level = require('./levels/' + ui.currentScene)

    return <div>
      <Scene className={cxs({zIndex : 10, textAlign : 'center', height: 0, overflow: 'visible'})}>
        {ui.fps}
      </Scene>
      <Level key={ui.currentScene} mode={ui.mode} />
    </div>
  }
))
export default App

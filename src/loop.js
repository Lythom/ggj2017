import {runInAction, action, autorun} from 'mobx'
import p2 from 'p2'

const WORLD_TO_PX = 10

import * as demoStore from 'src/demo/stores'
import * as mageStore from 'mage/stores'
import Keyboard from 'mage/inputs/Keyboard'
import * as Entity from 'mage/entity'
import * as Camera from 'mage/entities/Camera'
import * as AnimatedSprite from 'mage/components/AnimatedSprite'

import readIntentions, {MOVE_LEFT, MOVE_RIGHT, JUMP} from 'mage/inputs/intentions'
import mousePlateformer from 'mage/inputs/behaviours/mousePlateformer'
import keyboardPlateformer from 'mage/inputs/behaviours/keyboardPlateformer'

import {followPath, turnAround} from 'src/demo/entities/PlaceholderEntity'
import * as Spawner from 'mage/spawner';

import * as Tablecloth from 'src/demo/entities/Tablecloth'
import * as Table from 'src/demo/entities/Table'

const rate = (1000 / 60)

// Create a World
var world = new p2.World({
  gravity: [0, 10]
});

//world.solver.iterations = 10;
//world.solver.tolerance = 0.001;

const updateFromPhysics = Entity.mapIdsToEntities(function updateFromPhysics(entity) {
  const b = world.getBodyById(entity.bodyId)
  if (!b) return
  entity.x = ~~(b.position[0] * WORLD_TO_PX - entity.width / 2)
  entity.y = ~~(b.position[1] * WORLD_TO_PX - entity.height / 2)
})

export default () => Afloop(function loop(delta) {
  runInAction('gameloop', () => {

    const intentions = readIntentions(keyboardPlateformer, mousePlateformer)

    world.step(1 / 60, delta / 1000, 5)

    let ui = demoStore.ui
    const framesRate = 1000 / delta
    ui.fps = ~~((framesRate * 0.2) + (ui.fps * (1.0 - 0.2)))

    let table = mageStore.entities.get('table')
    let camera = mageStore.entities.get('camera')
    Camera.track('camera', 'table', 20, 0, -460)
    Tablecloth.initBodies('tablecloth', world)
    Table.initTableBody('table', world)

    updateFromPhysics('table')
    Spawner.bulkUpdate('tablecloth', updateFromPhysics)

    if (intentions[MOVE_LEFT]) {

    }

    /* let entity = mageStore.entities.get('mouseFollower')
     if (entity && (entity.x != demoStore.ui.mouseX || entity.y != demoStore.ui.mouseY)) {
     entity.x = demoStore.ui.mouseX
     entity.y = demoStore.ui.mouseY
     }

     turnAround('test', delta)

     if (mageStore.entities.has('test2')) {
     const test2 = mageStore.entities.get('test2')
     AnimatedSprite.step(test2, delta / (1000 / 24))
     //test2.x += 0.1
     }
     //turnAround('test2')

     followPath('freemovingtest', "thePath")
     entity = mageStore.entities.get('camera4')
     if (entity) {
     entity.rotate += 0.1
     }

     // white screen
     if (mageStore.entities.has('animatedSprite')) {
     entity = mageStore.entities.get('animatedSprite')
     if (intentions[MOVE_RIGHT]) {
     entity.accelerationX = entity.velocityX > 300 ? 0 : 1000
     entity.flipX = false
     } else if (intentions[MOVE_LEFT]) {
     entity.accelerationX = entity.velocityX < -300 ? 0 : -1000
     entity.flipX = true
     } else {
     entity.accelerationX = 0
     entity.velocityX = Math.abs(entity.velocityX) < 1 ? 0 : entity.velocityX / 1.2
     }
     if (intentions[JUMP] && !entity.isJumping) {
     entity.velocityY = -250
     entity.isJumping = true
     }
     if (entity.isJumping) {
     AnimatedSprite.transitionToAnimation(entity, 'jump')
     }
     else if (!intentions[MOVE_RIGHT] && !intentions[MOVE_LEFT]) {
     AnimatedSprite.transitionToAnimation(entity, 'iddle')
     } else if (intentions[MOVE_RIGHT] && entity.velocityX < 0) {
     AnimatedSprite.transitionToAnimation(entity, 'swap')
     entity.flipX = true
     } else if (intentions[MOVE_LEFT] && entity.velocityX > 0) {
     AnimatedSprite.transitionToAnimation(entity, 'swap')
     entity.flipX = false
     } else {
     AnimatedSprite.transitionToAnimation(entity, 'run')
     }

     Entity.updatePosition(entity, delta)
     AnimatedSprite.step(entity, Math.max((1000 / 60), delta) / (1000 / 24))
     // sprite rotation test
     // entity.rotate = entity.x / 1

     if (entity.y > 300) {
     entity.velocityY = 0
     entity.y = 300
     entity.isJumping = false
     }

     let entity = mageStore.entities.get('camera')
     Camera.track("camera", "animatedSprite", entity.velocityX / 2)
     //entity.y = ~~(mageStore.entities.get('animatedSprite').x * 5)
     /!* if (entity) {
     entity.zoom = 0.5 + mageStore.entities.get('animatedSprite').x / 1000
     entity.rotate = -mageStore.entities.get('animatedSprite').x / 10
     }*!/

     Spawner.randomlySpawn('charSpawner', 0.1, {y: 0, accelerationY : 0, velocityX : Math.random() * 600 - 300, velocityY : Math.random() * 600 - 300, animation : 'run'})
     Spawner.bulkUpdate('charSpawner', e => {
     AnimatedSprite.step(e, Math.max((1000 / 60), delta) / (1000 / 24))
     Entity.updatePosition(e, delta)
     })
     Spawner.bulkDestroy('charSpawner', e => e.x > 1000 || e.x < -1000 || e.y < -1000 || e.y > 1000)

     }
     */

  })
})

let loopTimeout = null
function Afloop(func) {
  let lastTime = window.performance.now()
  let currTime = 0
  clearInterval(loopTimeout)
  loopTimeout = setInterval(() => {
    currTime = window.performance.now()
    func(Math.min(currTime - lastTime, 1000))
    lastTime = currTime
  }, 1000 / 60)
}

if (module.hot) {
  module.hot.dispose(function () {
    clearInterval(loopTimeout)
  });
}
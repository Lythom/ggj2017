import {setMousePosition} from 'src/demo/stores/ui'

export default () => {
  let cssVarSupport = false // window.CSS && window.CSS.supports && window.CSS.supports('--fake-var', 0)
  if (cssVarSupport) {
    setMousePosition('var(--mouse-x)', 'var(--mouse-y)')
  }

  const root = document.documentElement;

  function onMouseMove(e) {
    let mousex = e.clientX
    let mousey = e.clientY
    if (e.changedTouches) {
      for (var i = 0; i < e.changedTouches.length; i++) {
        mousex = e.changedTouches[i].pageX
        mousey = e.changedTouches[i].pageY
      }
    }
    if (cssVarSupport) {
      root.style.setProperty('--mouse-x', mousex);
      root.style.setProperty('--mouse-y', mousey);
    } else {
      setMousePosition(mousex, mousey)
    }
  }

  function preventScroll(e) {
    e.preventDefault()
  }

  document.body.addEventListener('mousemove', onMouseMove)
  document.body.addEventListener('touchmove', onMouseMove)
  document.body.addEventListener('touchmove', preventScroll)

  if (module.hot) {
    module.hot.dispose(function() {
      document.body.removeEventListener('mousemove', onMouseMove)
      document.body.removeEventListener('touchmove', onMouseMove)
    });
  }
}
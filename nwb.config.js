var path = require('path')

module.exports = {
  type    : 'react-app',
  webpack : {
    aliases    : {
      'images' : path.resolve('src/assets/images'),
      'src'    : path.resolve('src'),
      'e2d'    : path.resolve('src/e2d.js'),
      'mage'   : path.resolve('src/mage')
    },
    publicPath : ''
  }
}

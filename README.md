# Molten Adamantium Game Engine #

An experimental game engine based to create games that runs on browsers using declarative rendering framework.

# Technical stack ideas #

## Done

* nwbloop
* Inferno
* Mobx + connect

## Excluded

* inferno-game-kit 
  * NO ! prefers reactive rendering vs. looped rendering : the loop should apply on data only, not components
  * components should not manage their data state directly, or just a small local state (same philosophy as webapp components)
    
## TODO

# features

## Done

* Scene switching example
* Main loop in a mobX transaction (https://github.com/ehgoodenough/afloop)
* 

## TODO

    => Spawner that displays the touches
        => The spawner is a hoc that encapsule the entity to mass create
        => The spawner itself is any entity,
        => The spawner have a function "trySpawn(spawner, spawnRate, initialProperties)"
        => The spawner have a function "spawn"
        => the spawner will take an array of entity data / ids
        => particleEmitter
        
    => TiledMap http://doc.mapeditor.org/reference/tmx-map-format/
        => <ObjectLayer map={require('map.json')} layerName="" />, place and init entities with the id (and eventually data) declared in the map
        => <TileLayer map={require('map.json')}, draw the tileset on a canvas. The tileset image is required automatically by the TileLayer
        => 

    
    => CanvasCamera
        https://medium.com/@joshuatenner/e2d-js-b77329a1a846#.ehnmblf6g
        https://gitter.im/e2d/e2d !!!!!!!
        le CanvasCamera pointe sur une camera et mimique sont comportement
        Le CanvasCamera prend comme children des fonctions e2d directement
        forceUpdate sur un onTick. retour de l'emitter ? NON pas de state locale, tout en state mobX, évolution via fonction externe
        https://github.com/ehgoodenough/snake-on-a-plane/blob/master/source/scripts/AspectRatioFrame.js
    => http://www.williammalone.com/articles/create-html5-canvas-javascript-sprite-animation/
        Tag, linked to camera entity ? <CanvasCamera id="camera" />
    
    
    
    => Gamepad handling
    
    => Canvas Grid

    => Cycle.js inpired approche ?
    - Isolate player input
    - Isolate output
    - isolate gameloop
    
    => Keybord
    - vkey ?
    -         https://github.com/ehgoodenough/keyb/blob/master/index.js

    
    => general utils + architecture
    - https://github.com/ehgoodenough/papercut/tree/master/source/scripts/utilities
    
    => Declaration
    la partie vue sert à la fois à définir le rendu, et à la fois à déclarer la liste des entités qui seront présentes. Entitiés statiques, Spawner(factory).
    - action spawn(spawnerId, factoryArgs)
    - factory = fonction qui prend des paramètres et retourne une entité. id auto généré ou accepté en factoryArgs.

    
    => Asset loading (image, music, sounds, tmx)
    https://webpack.js.org/concepts/
    http://blog.namangoel.com/css-in-js-in-css
    dataURI
    => collision detection
    => physics engine
    => networking
    => Animation (css transition, css animations, js choregraphies)
        => sprite animation
        - http://codepen.io/mindstorm/pen/aZZvKq
        -- To wrap into a parent to allow css transforms
        - https://github.com/frostney/react-spritesheet/blob/master/src/AnimatedSpriteSheet.js
        - http://stackoverflow.com/questions/35906196/improve-css3-background-position-animations-performance
    
    => Tweening
        http://tweenx.spheresofa.net/core/en.html#installation
        à compiler en JS




        http://www.piskelapp.com/


# Ideas

* 30 ticks/sec gameplay ?
* 20 ticks/sec gameplay with XXXfps rendering ? to match networking update rate ?

Entité : type + id => espace data réservé dans le store mobX

    entities[id] = {
        x,
        y,
        width,
        height,
        img,
        [physics],
        [shape]
    }
